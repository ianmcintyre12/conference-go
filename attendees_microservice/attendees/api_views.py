from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Attendee, ConferenceVO, AccountVO

# from events.models import Conference
import json


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class ListAttendeesEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
    ]


class ShowAttendeeEncoder(ModelEncoder):
    model = Attendee
    properties = ["email", "name", "company_name", "created"]

    encoders = {
        "conference": ConferenceVODetailEncoder(),
    }

    def get_extra_data(self, o):
        count = len(AccountVO.objects.filter(email=o.email))
        if count > 0:
            return {"has_account": True}
        else:
            return {"has_account": False}


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.

    {
        "attendees": [
            {
                "name": attendee's name,
                "href": URL to the attendee,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        attendees = Attendee.objects.filter(id=conference_vo_id)
        return JsonResponse(
            {"attendees": attendees}, encoder=ListAttendeesEncoder, safe=False
        )
    else:  # POST
        content = json.loads(request.body)
        try:
            conference_href = f"/api/conferences/{conference_vo_id}/"
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference ID"}, status=400
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(attendee, encoder=ShowAttendeeEncoder, safe=False)


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_attendee(request, id):
    """
    Returns the details for the Attendee model specified
    by the id parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    if request.method == "GET":
        try:
            person = Attendee.objects.get(id=id)
        except Attendee.DoesNotExist:
            return JsonResponse(
                {"message": "Person does not exist"}, status=400
            )
        return JsonResponse(person, encoder=ShowAttendeeEncoder, safe=False)
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"Deleted": count > 0})
    else:  # PUT
        new_info = json.loads(request.body)
        try:
            Attendee.objects.filter(id=id).update(**new_info)
        except Attendee.DoesNotExist:
            return JsonResponse(
                {"message": "Person does not exist"}, status=400
            )
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(attendee, encoder=ShowAttendeeEncoder, safe=False)
