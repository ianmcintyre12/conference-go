from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_picture_url(city, state):
    url = (
        "https://api.pexels.com/v1/search?query="
        + city
        + ","
        + state
        + "&per_page=1"
    )
    headers = {"Authorization": PEXELS_API_KEY}
    r = requests.get(url, headers=headers)
    search = r.json()
    try:
        img_url = search["photos"][0]["src"]["original"]
        img = {"picture_url": img_url}
        return img
    except (KeyError, IndexError):
        img = {"picture_url": None}
        return img


def get_weather_data(city, state):
    url = (
        "http://api.openweathermap.org/geo/1.0/direct?q="
        + city
        + ","
        + state
        + ",US"
        + "&limit=1&appid="
        + OPEN_WEATHER_API_KEY
    )
    r = requests.get(url)
    data = r.json()
    try:
        lat = str(data[0]["lat"])
        lon = str(data[0]["lon"])
    except (IndexError):
        weather_info = {"temp": "ERROR", "details": "ERROR"}
        return weather_info
    weather_url = (
        "https://api.openweathermap.org/data/2.5/weather?lat="
        + lat
        + "&lon="
        + lon
        + "&appid="
        + OPEN_WEATHER_API_KEY
        + "&units=imperial"
    )

    r_weather = requests.get(weather_url)
    weather_json = r_weather.json()
    weather_info = {
        "temp": str(weather_json["main"]["temp"]) + " F",
        "details": weather_json["weather"][0]["description"],
    }

    return weather_info
