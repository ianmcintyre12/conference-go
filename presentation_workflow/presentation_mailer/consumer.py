import json
import pika
import django
import os
import sys
from django.core.mail import send_mail
import time
from pika.exceptions import AMQPConnectionError

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_rejection(ch, method, properties, body):
    msg = json.loads(body)
    send_mail(
        "Your presentation has been rejected",
        f"{msg['presenter_name']}, we're sorry to tell you that your presentation {msg['title']} has been rejected",
        "admin@conference.go",
        [msg["presenter_email"]],
        fail_silently=False,
    )
    print("Rejection Email sent to " + msg["presenter_email"])


def process_approval(ch, method, properties, body):
    msg = json.loads(body)
    send_mail(
        "Your presentation has been accepted",
        f"{msg['presenter_name']}, we're happy to tell you that your presentation {msg['title']} has been accepted",
        "admin@conference.go",
        [msg["presenter_email"]],
        fail_silently=False,
    )
    print("Approval Email sent to " + msg["presenter_email"])


while True:
    try:

        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()

        # setting up approvals
        channel.queue_declare(queue="presentation_approvals")
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )

        # setting up rejections
        channel.queue_declare(queue="presentation_rejections")
        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        print("Connected to RabbitMQ")
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
